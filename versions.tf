terraform {

  # cloud {
  #   organization = "mc_2022"

  #   workspaces {
  #     name = "prod_mc_workspace"
  #   }
  # }

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.28.0"
    }
  }

  required_version = ">= 0.14.0"
}